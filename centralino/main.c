#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FASCIA_1 5
#define FASCIA_2 11
#define FASCIA_3 17
#define FASCIA_4 23
#define COSTO_FASCIA_1 0.23
#define COSTO_FASCIA_2 0.51
#define COSTO_FASCIA_3 0.65
#define COSTO_FASCIA_4 0.12
#define STR_MAX_LEN 60
#define STR_TEL_LEN 15+1
#define STR_DATA_LEN 10+1
#define STR_ORA_LEN 8+1


int main() {
    FILE* fp;
    char fileName[STR_MAX_LEN];
    char numero_interno[STR_TEL_LEN];
    char data[STR_DATA_LEN];
    char ora_inizio[STR_ORA_LEN];
    char ora_fine[STR_ORA_LEN];
    char tipo[2];
    char numero_interno_utente[STR_TEL_LEN];

    int mese;
    int mese_utente;

    int numeroChiamateUscita = 0;
    int numeroChiamateEntrata = 0;
    int secondiTotaleChiamateUscita = 0;
    int secondiTotaleChiamateEntrata = 0;
    int secondiTotaleChiamata = 0;
    int ora, min, sec;

    float costoTotaleChiamate;

    printf("Inserisci il nome del file: ");
    scanf("%s", fileName);

    printf("Inserisci il numero interno: ");
    scanf("%s", numero_interno_utente);

    printf("Inserisci il mese: ");
    scanf("%d", &mese_utente);

    if(mese_utente < 1 || mese_utente > 12){
        printf("Mese non valido\n");
        return -1;
    }

    fp = fopen(fileName, "r");

    if(fp == NULL){
        printf("ERROR");
        return -1;
    }
    while(fscanf(fp, "%s%*s%s%s%s%s", numero_interno, data, ora_inizio, ora_fine, tipo) != EOF){
        printf("%s %s %s %s %s\n", numero_interno, data, ora_inizio, ora_fine, tipo);

        sscanf(data, "%*d/%d/%*d", &mese);

        if(mese == mese_utente && strcmp(numero_interno, numero_interno_utente) == 0){
            printf("mese: %d, mese_utente: %d, numero_interno: %s, numero_interno_utente: %s\n",mese, mese_utente, numero_interno, numero_interno_utente);
            sscanf(ora_fine, "%d:%d:%d", &ora, &min, &sec);
            secondiTotaleChiamata = ora*3600 + min*60 + sec;
            sscanf(ora_inizio, "%d:%d:%d", &ora, &min, &sec);
            secondiTotaleChiamata = secondiTotaleChiamata - (ora*3600 + min*60 + sec);
            if(tipo[0] == 'U'){
                numeroChiamateUscita++;
                secondiTotaleChiamateUscita = secondiTotaleChiamateUscita + secondiTotaleChiamata;
                if(ora <= FASCIA_1){
                    costoTotaleChiamate = costoTotaleChiamate + ((secondiTotaleChiamata/60) * COSTO_FASCIA_1);
                } else if(ora <= FASCIA_2){
                    costoTotaleChiamate = costoTotaleChiamate + ((secondiTotaleChiamata/60) * COSTO_FASCIA_2);
                } else if(ora <= FASCIA_3){
                    costoTotaleChiamate = costoTotaleChiamate + ((secondiTotaleChiamata/60) * COSTO_FASCIA_3);
                } else {
                    costoTotaleChiamate = costoTotaleChiamate + ((secondiTotaleChiamata/60) * COSTO_FASCIA_4);
                }

            } else {
                numeroChiamateEntrata++;
                secondiTotaleChiamateEntrata = secondiTotaleChiamateEntrata + secondiTotaleChiamata;
            }
        }
    }

    fclose(fp);

    printf("Numero interno: %s\n", numero_interno_utente);
    printf("Costo totale telefonate: %.2f Euro\n", costoTotaleChiamate);
    printf("Numero delle chiamate in uscita: %d\n", numeroChiamateUscita);
    printf("Durate delle chiamate in uscita: %d minuti e %d secondi\n", secondiTotaleChiamateUscita/60, secondiTotaleChiamateUscita%60);
    printf("Numero chiamate in entrata: %d\n", numeroChiamateEntrata);
    printf("Durate delle chiamate in entrata: %d minuti e %d secondi\n", secondiTotaleChiamateEntrata/60, secondiTotaleChiamateEntrata%60);

    return 0;
}