#include <stdio.h>

int square(int x){
    return x*x;
}

double somma(double a, double b){
    return a+b;
}

void swap(int *a, int *b){
    int tmp;

    tmp = *a;
    *a = *b;
    *b = tmp;
}