#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LINE_LEN 81


int main() {
    FILE* fp;
    FILE* fpout;
    char nomeFile[MAX_LINE_LEN];
    char riga[MAX_LINE_LEN];
    char rigaCompressa[MAX_LINE_LEN] = "";
    int caratteriFileIn = 0;
    int caratteriFileOut = 0;
    int caratteriRigaOut;
    int i;
    char previus;
    int repetition = 0;
    char repetitionStr[MAX_LINE_LEN] = "";
    float ratio;

    printf("Inserisci il nome file: ");
    scanf("%s", nomeFile);

    fp = fopen(nomeFile, "r");
    fpout = fopen("output.txt", "w");

    if(fp == NULL){
        printf("ERROR");
        return -1;
    }

    if(fpout == NULL){
        printf("ERROR");
        return -1;
    }


    while(fscanf(fp, "%s", riga) != EOF){
        for (i = 0; i < strlen(riga); i++){
            if(i == 0){
                previus = riga[i];
            } else if(previus == riga[i]){
                repetition = repetition + 1;
            } else {
                strncat(rigaCompressa, &previus, 1);
                sprintf(repetitionStr, "%d", repetition);
                strncat(rigaCompressa, repetitionStr, strlen(repetitionStr));
                previus = riga[i];
                repetition = 1;
            }
            caratteriFileIn++;
        }
        strncat(rigaCompressa, &previus, 1);
        sprintf(repetitionStr, "%d", repetition);
        strncat(rigaCompressa, repetitionStr, strlen(repetitionStr));
        caratteriRigaOut = fprintf(fpout, "%s\n", rigaCompressa);
        caratteriFileOut = caratteriFileOut + caratteriRigaOut;
        strcpy(rigaCompressa, "");
        repetition = 1;
    }

    fclose(fp);
    fclose(fpout);
    ratio = ((float) caratteriFileOut / caratteriFileIn) * 100;
    printf("File originale: %d caratteri\n", caratteriFileIn);
    printf("File compresso: %d caratteri\n", caratteriFileOut);
    printf("Compressione: %.2f\n", ratio);
    return 0;
}